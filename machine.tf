resource "digitalocean_droplet" "demo-server-orchard" {
  image = "centos-7-x64"
  name = "demo-server-orchard"
  region = "fra1"
  size = "s-1vcpu-1gb"
  private_networking = true
  tags = [
    "demo-server",
    "gitlab",
    "orchard"
  ]
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }
  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "yum update -y",
      "yum install -y yum-utils",
      "yum install -y https://repo.ius.io/ius-release-el7.rpm",
      "yum install -y git222",
      "yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin",
      "systemctl start docker",
      "curl -SL https://github.com/docker/compose/releases/download/v2.18.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose",
      "ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose",
      "git clone https://gitlab.com/nedbal.attila/orchard-course.git",
      "cd orchard-course && docker-compose up -d"
    ]
  }
}
